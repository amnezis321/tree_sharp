﻿// See https://aka.ms/new-console-template for more information
using tree;

/*Console.WriteLine("Hello, World!");
double result = MathExprIntepreter.Execute("3+4*(2+7-3)+5*10");
Console.WriteLine(result);*/
TextReader reader =
 args.Length >= 1 ? new StreamReader(args[0])
 : Console.In;
String source = reader.ReadToEnd();
try
{
    AstNode program = MathLangParser.Parse(source);
    AstNodePrinter.Print(program);
    Console.WriteLine("------------------------");
    MathLangIntepreter.Execute(program);
}
catch (Exception e)
{
    Console.WriteLine("Error: {0}", e);
}
Console.ReadLine();
