﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using tree;

namespace tree
{
    public class MathLangParser : ParserBase
    {
        // конструктор
        public MathLangParser(string source)
        : base(source)
        {
        }
        // далее идет реализация в виде функций правил грамматики
        // NUMBER -> <число>
        public AstNode NUMBER()
        {
            string number = "";
            while (Current == '.' || char.IsDigit(Current))
            {
                number += Current;

                Next();
            }
            if (number.Length == 0)
                throw new ParserBaseException(
                string.Format("Ожидалось число (pos={0})", Pos));
            Skip();
            return new AstNode(AstNodeType.NUMBER, number);
        }
        // IDENT -> <идентификатор>
        public AstNode IDENT()
        {
            string identifier = "";
            if (char.IsLetter(Current))
            {
                identifier += Current;
                Next();
                while (char.IsLetterOrDigit(Current))
                {
                    identifier += Current;
                    Next();
                }
            }
            else
                throw new ParserBaseException(
                string.Format("Ожидался идентификатор (pos={0})", Pos));
            Skip();
            return new AstNode(AstNodeType.IDENT, identifier);
        }
        // group -> "(" term ")" | IDENT | NUMBER
        public AstNode Group()
        {
            if (IsMatch("("))
            { // выбираем альтернативу
                Match("("); // это выражение в скобках
                AstNode result = Term();
                Match(")");
                return result;
            }
            else if (char.IsLetter(Current))
            {
                int pos = Pos; // это идентификатор
                return IDENT();
            }
            else
                return NUMBER(); // число
        }
        // mult -> group ( ( "*" | "/" ) group )*
        public AstNode Mult()
        {
            AstNode result = Group();
            while (IsMatch("*", "/"))
            { // повторяем нужное кол-во раз

                string oper = Match("*", "/"); // здесь выбор альтернативы
                AstNode temp = Group(); // реализован иначе
                result =
                oper == "*" ? new AstNode(AstNodeType.MUL, result, temp)
                : new AstNode(AstNodeType.DIV, result, temp);
            }
            return result;
        }
        // add -> mult ( ( "+" | "-" ) mult )*
        public AstNode Add()
        { // реализация аналогично правилу mult
            AstNode result = Mult();
            while (IsMatch("+", "-"))
            {
                string oper = Match("+", "-");
                AstNode temp = Mult();
                result =
                oper == "+" ? new AstNode(AstNodeType.ADD, result, temp)
                : new AstNode(AstNodeType.SUB, result, temp);
            }
            return result;
        }
        // term -> add
        public AstNode Term()
        {
            return Add();
        }
        // expr -> "print" term | "input" IDENT | IDENT "=" term
        public AstNode Expr()
        {
            if (IsMatch("print"))
            { // выбираем альтернативу
                Match("print"); // это вывод данных
                AstNode value = Term();
                return new AstNode(AstNodeType.PRINT, value);
            }
            else if (IsMatch("input"))
            {
                Match("input"); // это ввод данных
                AstNode identifier = IDENT();
                return new AstNode(AstNodeType.INPUT, identifier);
            }
            else
            {
                AstNode identifier = IDENT();
                Match("="); // это операция присвоения значения
                AstNode value = Term();
                return new AstNode(AstNodeType.ASSIGN, identifier, value);
            }
        }
        // program -> ( expr )*
        public AstNode Program()
        {

            AstNode programNode = new AstNode(AstNodeType.PROGRAM);
            while (!End) // повторяем до конца входной строки
                programNode.AddChild(Expr());
            return programNode;
        }
        // result -> program
        public AstNode Result()
        {
            return Program();
        }
        // метод, вызывающий начальное и правило грамматики и
        // соответствующий парсинг
        public AstNode Parse()
        {
            Skip();
            AstNode result = Result();
            if (End)
                return result;
            else
                throw new ParserBaseException( // разобрали не всю строку
                string.Format("Лишний символ '{0}' (pos={1})",
                Current, Pos)
                );
        }
        // статическая реализации предыдузего метода (для удобства)
        public static AstNode Parse(string source)
        {
            MathLangParser mlp = new MathLangParser(source);
            return mlp.Parse();
        }
    }
}